# Aufgabe 1: Packet Tracer - Navigation im IOS

## Inhalt
- [Aufgabe 1: Packet Tracer - Navigation im IOS](#aufgabe-1-packet-tracer---navigation-im-ios)
  - [Inhalt](#inhalt)
  - [Ziele](#ziele)
  - [Hintergrund / Szenario](#hintergrund--szenario)
  - [Anweisungen](#anweisungen)
    - [Teil 1: Grundlegende Verbindungen herstellen, auf die CLI zugreifen und Hilfe erkunden](#teil-1-grundlegende-verbindungen-herstellen-auf-die-cli-zugreifen-und-hilfe-erkunden)
      - [Schritt 1: Verbinden Sie PC1 mit S1 über ein Konsolenkabel.](#schritt-1-verbinden-sie-pc1-mit-s1-über-ein-konsolenkabel)
      - [Schritt 2: Eine Terminal-Sitzung mit S1 herstellen.](#schritt-2-eine-terminal-sitzung-mit-s1-herstellen)
      - [Schritt 3: Die IOS-Hilfe erkunden.](#schritt-3-die-ios-hilfe-erkunden)
    - [Teil 2: EXEC-Modi erkunden](#teil-2-exec-modi-erkunden)
      - [Schritt 1: In den privilegierten EXEC-Modus wechseln.](#schritt-1-in-den-privilegierten-exec-modus-wechseln)
      - [Schritt 2: In den globalen Konfigurationsmodus wechseln](#schritt-2-in-den-globalen-konfigurationsmodus-wechseln)
    - [Teil 3: Die Uhr einstellen](#teil-3-die-uhr-einstellen)
      - [Schritt 1: Den Befehl clock verwenden.](#schritt-1-den-befehl-clock-verwenden)
      - [Schritt 2: Zusätzliche Befehlsnachrichten erkunden.](#schritt-2-zusätzliche-befehlsnachrichten-erkunden)

## Ziele
- **Teil 1**: Grundlegende Verbindungen herstellen, auf die CLI zugreifen und Hilfe erkunden
- **Teil 2**: EXEC-Modi erkunden
- **Teil 3**: Die Uhr einstellen

## Hintergrund / Szenario
In dieser Aktivität üben Sie Fähigkeiten, die für die Navigation im Cisco IOS notwendig sind, wie verschiedene Benutzerzugriffsmodi, verschiedene Konfigurationsmodi und häufig verwendete Befehle. Sie üben auch den Zugriff auf die kontextsensitive Hilfe durch Konfigurieren des Uhr-Befehls.

## Anweisungen

### Teil 1: Grundlegende Verbindungen herstellen, auf die CLI zugreifen und Hilfe erkunden

#### Schritt 1: Verbinden Sie PC1 mit S1 über ein Konsolenkabel.
  a. Klicken Sie auf das Verbindungen-Symbol (das wie ein Blitz aussieht) in der unteren linken Ecke des Packet Tracer-Fensters.
  b. Wählen Sie das hellblaue Konsolenkabel durch Klicken darauf aus. Der Mauszeiger ändert sich zu einem Stecker mit einem herabhängenden Kabel.
  c. Klicken Sie auf PC1. Ein Fenster zeigt eine Option für eine RS-232-Verbindung. Verbinden Sie das Kabel mit dem RS-232-Port.
  d. Ziehen Sie das andere Ende der Konsolenverbindung zum S1-Switch und klicken Sie auf den Switch, um die Verbindungsliste aufzurufen.
  e. Wählen Sie den Konsolenport, um die Verbindung zu vervollständigen.

#### Schritt 2: Eine Terminal-Sitzung mit S1 herstellen.
  a. Klicken Sie auf PC1 und dann auf den Reiter „Desktop“.
  b. Klicken Sie auf das Symbol der Terminal-Anwendung. Überprüfen Sie, ob die Standardeinstellungen der Portkonfiguration korrekt sind.

**Frage: Wie lautet die Einstellung für Bits pro Sekunde?**  
Antwort: Die Standardeinstellung für Bits pro Sekunde ist typischerweise 9600.

  c. Auf dem Bildschirm werden möglicherweise mehrere Nachrichten angezeigt. Irgendwo auf dem Bildschirm sollte eine Nachricht „Press RETURN to get started!“ erscheinen. Drücken Sie ENTER.

**Frage: Wie lautet die Anzeige auf dem Bildschirm?**  
Antwort: Die Anzeige fordert den Benutzer auf, die Eingabetaste zu drücken, um zu beginnen.

#### Schritt 3: Die IOS-Hilfe erkunden.
  a. Das IOS kann Hilfe für Befehle anbieten, je nachdem, auf welcher Ebene zugegriffen wird. Die derzeit angezeigte Aufforderung heißt User EXEC, und das Gerät wartet auf einen Befehl. Die einfachste Form der Hilfe besteht darin, ein Fragezeichen (?) an der Aufforderung einzugeben, um eine Liste von Befehlen anzuzeigen.

`S1> ?`

**Frage: Welcher Befehl beginnt mit dem Buchstaben 'C'?**  
Antwort: Ein Beispiel für einen Befehl, der mit 'C' beginnt, ist `connect`.

  b. Geben Sie am Prompt 't' ein und dann ein Fragezeichen (?).

`S1> t?`

**Frage: Welche Befehle werden angezeigt?**  
Antwort: Befehle wie `telnet` oder `traceroute` könnten angezeigt werden.

  Geben Sie am Prompt 'te' ein und dann ein Fragezeichen (?).

`S1> te?`

**Frage: Welche Befehle werden angezeigt?**  
Antwort: Möglicherweise werden Befehle wie `terminal` angezeigt.

Diese Art von Hilfe wird als kontextsensitive Hilfe bezeichnet. Sie bietet mehr Informationen, wenn die Befehle erweitert werden.

### Teil 2: EXEC-Modi erkunden
In Teil 2 dieser Aktivität wechseln Sie in den privilegierten EXEC-Modus und geben zusätzliche Befehle ein.

#### Schritt 1: In den privilegierten EXEC-Modus wechseln.
  a. Geben Sie am Prompt das Fragezeichen (?) ein.

`S1> ?`

**Frage: Welche Informationen werden für den Befehl enable angezeigt?**  
Antwort: Der `enable` Befehl versetzt den Benutzer in den privilegierten EXEC-Modus.

  b. Tippen Sie 'en' ein und drücken Sie die Tab-Taste.

`S1> en<Tab>`

**Frage: Was wird angezeigt, nachdem die Tab-Taste gedrückt wurde?**  
Antwort: Der `enable` Befehl wird automatisch vervollständigt.

  c. Geben Sie den Befehl enable ein und drücken Sie ENTER.

**Frage: Wie ändert sich die Aufforderung?**  
Antwort: Die Aufforderung ändert sich von `S1>` zu `S1#`, was anzeigt, dass der Benutzer nun im privilegierten EXEC-Modus ist.

  d. Geben Sie bei Aufforderung das Fragezeichen (?) ein.

`S1# ?`

**Frage: Wie viele Befehle werden jetzt angezeigt, da der privilegierte EXEC-Modus aktiv ist?**  
Antwort: Die genaue Anzahl kann variieren, aber es werden mehr Befehle als im User EXEC-Modus angezeigt.

#### Schritt 2: In den globalen Konfigurationsmodus wechseln
  a. Wenn Sie sich im privilegierten EXEC-Modus befinden, ist einer der Befehle, die mit dem Buchstaben 'C' beginnen, configure. Geben Sie entweder den vollständigen Befehl oder genug vom Befehl ein, um ihn eindeutig zu machen. Drücken Sie die <Tab>-Taste, um den Befehl auszuführen, und drücken Sie ENTER.

`S1# configure`

**Frage: Welche Nachricht wird angezeigt?**  
Antwort: Eine Aufforderung zur Eingabe des Konfigurationsmodus, typischerweise mit der Option, in den terminal-basierten Konfigurationsmodus zu wechseln.

  b. Drücken Sie Enter, um den Standardparameter, der in Klammern [terminal] angegeben ist, zu akzeptieren.

**Frage: Wie ändert sich die Aufforderung?**  
Antwort: Die Aufforderung ändert sich zu `S1(config)#`, was anzeigt, dass der Benutzer nun im globalen Konfigurationsmodus ist.

  c. Dies wird als globaler Konfigurationsmodus bezeichnet. Dieser Modus wird in zukünftigen Aktivitäten und Laboren weiter erkundet. Kehren Sie jetzt zum privilegierten EXEC-Modus zurück, indem Sie 'end', 'exit' oder Ctrl-Z eingeben.

`S1(config)# exit`

`S1#`

### Teil 3: Die Uhr einstellen
#### Schritt 1: Den Befehl clock verwenden.
  a. Verwenden Sie den Befehl clock, um Hilfe und Befehlssyntax weiter zu erkunden. Geben Sie am privilegierten EXEC-Prompt 'show clock' ein.

`S1# show clock`

**Frage: Welche Informationen werden angezeigt? Welches Jahr wird angezeigt?**  
Antwort: Der Befehl zeigt die aktuelle Uhrzeit und das Datum an. Das angezeigte Jahr hängt von der aktuellen Systemkonfiguration ab.

  b. Verwenden Sie die kontextsensitive Hilfe und den Befehl clock, um die Zeit auf dem Switch auf die aktuelle Zeit einzustellen. Geben Sie den Befehl clock ein und drücken Sie ENTER.

`S1# clock<ENTER>`

**Frage: Welche Informationen werden angezeigt?**  
Antwort: Eine Nachricht, dass der Befehl unvollständig ist und weitere Angaben benötigt werden.

  c. Die Nachricht „% Incomplete command“ wird vom IOS zurückgegeben. Dies bedeutet, dass dem Befehl clock weitere Parameter fehlen. Jedes Mal, wenn mehr Informationen benötigt werden, kann Hilfe angefordert werden, indem nach dem Befehl ein Leerzeichen und das Fragezeichen (?) eingegeben wird.

`S1# clock ?`

**Frage: Welche Informationen werden angezeigt?**  
Antwort: Die verfügbaren Optionen für den `clock` Befehl werden angezeigt, einschließlich der Möglichkeit, die Uhrzeit mit `set` einzustellen.

  d. Stellen Sie die Uhr mit dem Befehl clock set ein. Gehen Sie den Befehl Schritt für Schritt durch.

`S1# clock set ?`

**Fragen: Welche Informationen werden angefordert?**  
Antwort: Informationen zu Stunde, Minute, Sekunde, Tag, Monat und Jahr werden angefordert.

**Was wäre angezeigt worden, wenn nur der Befehl clock set eingegeben worden wäre und keine Hilfe mit dem Fragezeichen angefordert wurde?**  
Antwort: Das System hätte wahrscheinlich eine Fehlermeldung ausgegeben oder spezifische Anweisungen für die erforderlichen Zeit- und Datumsangaben bereitgestellt.

  e. Basierend auf den Informationen, die durch den Befehl clock set ? angefordert wurden, geben Sie eine Zeit von 15:00 Uhr im 24-Stunden-Format ein. Überprüfen Sie, ob weitere Parameter benötigt werden.

`S1# clock set 15:00:00 ?`

Die Ausgabe fordert weitere Informationen an:

<1-31> Tag des Monats

MONTH Monat des Jahres

  f. Versuchen Sie, das Datum auf den 31.01.2035 mit dem angeforderten Format einzustellen. Es kann notwendig sein, zusätzliche Hilfe durch kontextsensitive Hilfe anzufordern, um den Prozess abzuschließen. Geben Sie danach den Befehl 'show clock' ein, um die Uhreinstellung anzuzeigen. Das resultierende Befehlsergebnis sollte wie folgt aussehen:

`S1# show clock`

*15:0:4.869 UTC Dienstag, 31. Januar 2035

  g. Wenn Sie nicht erfolgreich waren, versuchen Sie den folgenden Befehl, um das oben genannte Ergebnis zu erhalten:

`S1# clock set 15:00:00 31 Jan 2035`

#### Schritt 2: Zusätzliche Befehlsnachrichten erkunden.
  a. Das IOS liefert verschiedene Ausgaben für falsche oder unvollständige Befehle. Setzen Sie die Verwendung des Befehls clock fort, um weitere Nachrichten zu erkunden, die Ihnen begegnen können, während Sie das IOS nutzen.

  b. Geben Sie die folgenden Befehle ein und notieren Sie die Nachrichten:

`S1# cl<tab>`

**Fragen: Welche Informationen wurden zurückgegeben?**  
Antwort: Der `clock` Befehl wird vervollständigt, wenn `cl<tab>` eingegeben wird, da es der einzige Befehl ist, der mit `cl` beginnt.