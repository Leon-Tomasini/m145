# Netzwerkdokumentation
- Wie lautet die Netzwerkadresse vom Standort Samedan?

    192.168.3.0/24

- Auf welcher IP-Adresse befindet sich der AccessPoint in Bellinzona?

    192.168.4.30/24

- In welchem VLAN befinden sich die Arbeitsplatz-PCs?

    VLAN-2

- Über welche IP-Adresse erreicht man den Manageable-Switch am Standort Chur?

    192.168.2.253/24

- Welche IP-Adressen LAN-seitig und tunnelseitig ist der VPN-Gateway am Standort Bellinzona konfiguriert?

    172.200.4.2/24

- Wie lautet der am Standort Samedan an den Arbeitsplatz-PCs eingetragene Standardgateway?

    192.168.3.1/24

- Ein Aussendienstmitarbeiter benötigt Zugriff auf das Firmennetzwerk. Wie muss er seine VPN-SW IP-mässig konfigurieren?

    172.200.5.1/24 (VPN-C)

- Wer hat im Büro CAD Wasserbau die VoIP-Telefone installiert?

    abisang

- Wann wurde im Bistro der AccesPoint installiert?

    23.07.2014

- Der IT-Mitarbeiter Rene Sauter (rsauter) verlässt die Firma. Welche Arbeiten bzw. Verantwortlichkeiten sind davon betroffen? Wen würden sie als zukünftigen Ansprechpartner bei Problemen vorschlagen?

    blaeuchli oder rkundert

- Wieviele RJ45-Steckdosen stehen ihnen im Bauleiterbüro zur Verfügung und wieviele davon sind zurzeit noch nicht belegt?

    Es gibt insgesamt 28 RJ45-Steckdosen und davon sind 6 noch frei

- Im Büro CAD Tiefbau muss ein weiteres VoIP-Telefon zur Verfügung stehen. Wie soll diese Verbindung gepatched werden? Verlangt wird die Patchpanelbelegung und der Switch-Port.

    E2/2

- Im Bistro soll eine Projektpräsentation stattfinden. Dafür muss ein Ethernetkabelverbindung bereitgestellt werden.

    Hier könnte man die Ports E8/2, E9/1 oder E9/2 verwenen

- Im Büro CAD Wasserbau soll temporär ein weiterer Arbeitsplatz eingereichtet werden. Wie werden sie diese Aufgabe lösen?

    Da schon bereits alle acht RJ45-Anschlüsse belegt sind werde ich dies ablehnen
    Evtl. könnte man jedoch noch ein extra langes Ethernet-Kabel direkt vom Patchpanel ziehen

- Wieviele Switchs stehen ihnen am Standort Chur zur Verfügung?

    Zwei

- Frau Sommer arbeitet an der CAD-Workstation und meldet ihnen, dass sie keine Verbindung ins Internet mehr hat. Was werden sie überprüfen?

    Zuerst wird überprüft, ob alle Kabel richtig stecken. Folgend werden die Internet Einstellungen gecheckt. Falls dies immer noch nicht hilft, dann den PC neustarten und ansonsten beim Serverrack nachsehen.

- Wie lautet die SSID des Churer-AccessPoint?

    David (Habe nirgends eine SSID gefunden)