# Aufgabe 4: Packet Tracer - Identifizierung von MAC- und IP-Adressen

## Inhalt
- [Aufgabe 4: Packet Tracer - Identifizierung von MAC- und IP-Adressen](#aufgabe-4-packet-tracer---identifizierung-von-mac--und-ip-adressen)
  - [Inhalt](#inhalt)
  - [Ziele](#ziele)
  - [Hintergrund](#hintergrund)
  - [Anweisungen](#anweisungen)
    - [Teil 1: PDU-Informationen für lokale Netzwerkkommunikation sammeln](#teil-1-pdu-informationen-für-lokale-netzwerkkommunikation-sammeln)
      - [Schritt 1: Lokale PDU-Analyse und -Nachverfolgung](#schritt-1-lokale-pdu-analyse-und--nachverfolgung)
      - [Schritt 2: Sammeln Sie zusätzliche PDU-Informationen aus anderen Pings](#schritt-2-sammeln-sie-zusätzliche-pdu-informationen-aus-anderen-pings)
    - [Teil 2: PDU-Informationen für entfernte Netzwerkkommunikation sammeln](#teil-2-pdu-informationen-für-entfernte-netzwerkkommunikation-sammeln)
      - [Schritt 1: Sammeln Sie PDU-Informationen, während ein Paket von 172.16.31.5 zu 10.10.10.2 reist](#schritt-1-sammeln-sie-pdu-informationen-während-ein-paket-von-17216315-zu-1010102-reist)
    - [Schritt 2: Sammeln Sie die gleichen Informationen von Schritt 1d](#schritt-2-sammeln-sie-die-gleichen-informationen-von-schritt-1d)
  - [Reflexionsfragen und Antworten](#reflexionsfragen-und-antworten)

## Ziele
- **Teil 1**: PDU-Informationen für lokale Netzwerkkommunikation sammeln
- **Teil 2**: PDU-Informationen für entfernte Netzwerkkommunikation sammeln

## Hintergrund
Diese Aktivität ist optimiert, um PDUs (Protocol Data Units) zu betrachten. Die Geräte sind bereits konfiguriert. Sie sammeln PDU-Informationen im Simulationsmodus und beantworten eine Reihe von Fragen zu den gesammelten Daten.

## Anweisungen

### Teil 1: PDU-Informationen für lokale Netzwerkkommunikation sammeln

#### Schritt 1: Lokale PDU-Analyse und -Nachverfolgung

- **Hinweis**: Überprüfen Sie die Reflexionsfragen in Teil 3, bevor Sie mit Teil 1 fortfahren. Es gibt Ihnen eine Vorstellung von der Art der Informationen, die Sie sammeln müssen. Sammeln Sie PDU-Informationen, während ein Paket von 172.16.31.5 zu 172.16.31.2 reist.

  a. Klicken Sie auf 172.16.31.5 und öffnen Sie die Eingabeaufforderung.

  b. Geben Sie den Befehl `ping 172.16.31.2` ein.

  c. Wechseln Sie in den Simulationsmodus und wiederholen Sie den Befehl `ping 172.16.31.2`. Eine PDU erscheint neben 172.16.31.5.

  d. Klicken Sie auf die PDU und notieren Sie die folgenden Informationen aus den Tabs "OSI Model" und "Outbound PDU Layer":

     - Ziel-MAC-Adresse: 000C:85CC:1DA7
     - Quell-MAC-Adresse: 00D0:D311:C788
     - Quell-IP-Adresse: 172.16.31.5
     - Ziel-IP-Adresse: 172.16.31.2
     - Bei Gerät: 172.16.31.5

  e. Klicken Sie auf "Capture / Forward" (der rechte Pfeil gefolgt von einer vertikalen Linie), um die PDU zum nächsten Gerät zu bewegen. Sammeln Sie die gleichen Informationen von Schritt 1d. Wiederholen Sie diesen Prozess, bis die PDU ihr Ziel erreicht. Zeichnen Sie die gesammelten PDU-Informationen in eine Tabelle ähnlich dem unten gezeigten Beispiel auf:

| Bei Gerät    | Ziel-MAC       | Quell-MAC     | Quell-IPv4    | Ziel-IPv4     |
|--------------|----------------|---------------|---------------|---------------|
| 172.16.31.5  | 000C:85CC:1DA7 | 00D0:D311:C788| 172.16.31.5   | 172.16.31.2   |
| Switch1      | 000C:85CC:1DA7 | 00D0:D311:C788| N/A           | N/A           |
| Hub          | N/A            | N/A           | N/A           | N/A           |
| 172.16.31.2  | 00D0:D311:C788 | 000C:85CC:1DA7| 172.16.31.2   | 172.16.31.5   |

#### Schritt 2: Sammeln Sie zusätzliche PDU-Informationen aus anderen Pings

Wiederholen Sie den Prozess aus Schritt 1 und sammeln Sie die Informationen für die folgenden Tests:

- Ping 172.16.31.2 von 172.16.31.3.
- Ping 172.16.31.4 von 172.16.31.5.

Kehren Sie in den Echtzeitmodus zurück.

### Teil 2: PDU-Informationen für entfernte Netzwerkkommunikation sammeln

Um mit entfernten Netzwerken zu kommunizieren, ist ein Gateway-Gerät notwendig. Studieren Sie den Prozess, der stattfindet, um mit Geräten im entfernten Netzwerk zu kommunizieren. Achten Sie besonders auf die verwendeten MAC-Adressen.

#### Schritt 1: Sammeln Sie PDU-Informationen, während ein Paket von 172.16.31.5 zu 10.10.10.2 reist

a. Klicken Sie auf 172.16.31.5 und öffnen Sie die Eingabeaufforderung.  
b. Geben Sie den Befehl `ping 10.10.10.2` ein.  
c. Wechseln Sie in den Simulationsmodus und wiederholen Sie den Befehl `ping 10.10.10.2`. Eine PDU erscheint neben 172.16.31.5.  
d. Klicken Sie auf die PDU und notieren Sie die folgenden Informationen aus dem Tab "Outbound PDU Layer":  

   - Ziel-MAC-Adresse: 00D0:BA8E:741A  
   - Quell-MAC-Adresse: 00D0:D311:C788  
   - Quell-IP-Adresse: 172.16.31.5  
   - Ziel-IP-Adresse: 10.10.10.2  
   - Bei Gerät: 172.16.31.5  

**Frage**: Welches Gerät hat die angezeigte Ziel-MAC-Adresse?

### Schritt 2: Sammeln Sie die gleichen Informationen von Schritt 1d

| Bei Gerät   | Ziel-MAC       | Quell-MAC     | Quell-IPv4   | Ziel-IPv4    |
|-------------|----------------|---------------|--------------|--------------|
| 172.16.31.5 | 00D0:BA8E:741A | 00D0:D311:C788| 172.16.31.5  | 10.10.10.2   |
| Switch1     | 00D0:BA8E:741A | 00D0:D311:C788| N/A          | N/A          |
| Router      | 0060:2F84:4AB6 | 00D0:588C:2401| 172.16.31.5  | 10.10.10.2   |
| Switch0     | 0060:2F84:4AB6 | 00D0:588C:2401| N/A          | N/A          |
| Access Point| N/A            | N/A           | N/A          | N/A          |
| 10.10.10.2  | 00D0:588C:2401 | 0060:2F84:4AB6| 10.10.10.2   | 172.16.31.5  |

## Reflexionsfragen und Antworten

1. **Wurden verschiedene Arten von Kabeln/Medien verwendet, um Geräte zu verbinden?**  
   A: Ja, in Netzwerksimulationen können verschiedene Kabeltypen (z.B. Kupferkabel, Glasfaserkabel, drahtlose Verbindungen) verwendet werden, abhängig von den zu verbindenden Gerätetypen und den Netzwerkanforderungen.

2. **Haben die Kabel die Handhabung der PDU in irgendeiner Weise verändert?**  
   A: Nein, die Art des Kabels oder Mediums ändert nicht die Handhabung der PDU selbst, aber es kann die Übertragungsgeschwindigkeit und -reichweite beeinflussen.

3. **Hat der Hub irgendwelche Informationen verloren, die er erhalten hat?**  
   A: Ein Hub sollte keine Informationen verlieren; er leitet PDUs einfach an alle anderen Ports weiter, ohne die Daten zu filtern oder zu analysieren.

4. **Was macht der Hub mit MAC-Adressen und IP-Adressen?**  
   A: Ein Hub unterscheidet nicht zwischen MAC- oder IP-Adressen, da er auf der OSI-Schicht 1 (physische Schicht) arbeitet und lediglich Signale verstärkt und an alle Ports weiterleitet.

5. **Hat der drahtlose Zugangspunkt etwas mit den gegebenen Informationen gemacht?**  
   A: Ein drahtloser Zugangspunkt leitet Daten zwischen einem kabelgebundenen Netzwerk und drahtlosen Geräten weiter. Er kann MAC-Adressen für die Steuerung des Netzwerkzugangs verwenden, verändert jedoch die Inhalte der PDUs nicht.

6. **Wurde irgendeine MAC- oder IP-Adresse während der drahtlosen Übertragung verloren?**  
   A: Nein, MAC- und IP-Adressen sollten während der Übertragung nicht verloren gehen, da sie für die korrekte Zustellung der PDU entscheidend sind.

7. **Welche war die höchste OSI-Schicht, die der Hub und Zugangspunkt verwendet haben?**  
   A: Der Hub arbeitet auf Schicht 1 (physische Schicht), während ein Zugangspunkt bis zur Schicht 2 (Datensicherheitsschicht) arbeiten kann, da er MAC-Adressen für den Netzwerkzugang verwendet.

8. **Haben der Hub oder Zugangspunkt jemals eine PDU repliziert, die mit einem roten „X“ abgelehnt wurde?**  
   A: Hubs und Zugangspunkte replizieren alle eingehenden PDUs unabhängig von deren Zustand; sie unterscheiden nicht zwischen erfolgreichen oder fehlgeschlagenen Übertragungen.

9. **Beim Betrachten des PDU-Details-Reiters, welche MAC-Adresse erschien zuerst, die Quelle oder das Ziel?**  
   A: Die Quell-MAC-Adresse erscheint zuerst, gefolgt von der Ziel-MAC-Adresse.

10. **Warum erscheinen die MAC-Adressen in dieser Reihenfolge?**  
    A: Dies folgt dem Standardformat von Ethernet-Rahmen, wo die Quelladresse auf die Zieladresse folgt, um die Identifikation des Senders und Empfängers zu ermöglichen.

11. **Gab es ein Muster in der MAC-Adressierung in der Simulation?**  
    A: Ja, MAC-Adressen folgen einem spezifischen Format und können dazu verwendet werden, den Hersteller des Netzwerkgeräts zu identifizieren.

12. **Haben die Switches jemals eine PDU repliziert, die mit einem roten „X“ abgelehnt wurde?**  
    A: Switches leiten PDUs basierend auf MAC-Adresstabellen weiter. Wenn eine PDU nicht zugestellt werden kann (angezeigt durch ein rotes „X“), ist dies in der Regel auf ein Problem in der Netzwerkkonfiguration zurückzuführen, nicht auf die Handlung des Switches selbst.

13. **Jedes Mal, wenn die PDU zwischen dem 10er-Netzwerk und dem 172er-Netzwerk gesendet wurde, gab es einen Punkt, an dem die MAC-Adressen plötzlich wechselten. Wo trat das auf?**  
    A: Dies trat am Router auf, der als Gateway zwischen den beiden Netzwerken dient. Der Router ändert die Quell- oder Ziel-MAC-Adresse, da er Pakete zwischen verschiedenen Netzwerken weiterleitet.

14. **Welches Gerät verwendet MAC-Adressen, die mit 00D0:BA beginnen?**  
    A: Dies könnte ein Gerät eines bestimmten Herstellers sein, basierend auf der eindeutigen Zuordnung von MAC-Adressbereichen zu Herstellern. Die spezifische Identifikation erfordert eine Suche in einer MAC-Adressdatenbank.

15. **Zu welchen Geräten gehörten die anderen MAC-Adressen?**  
    A: Ähnlich wie bei Frage 14 müssten die MAC-Adressen mit einer Datenbank abgeglichen werden, um die spezifischen Geräte oder Hersteller zu identifizieren.

16. **Haben sich die sendenden und empfangenden IPv4-Adressen in irgendeinem der PDUs geändert?**  
    A: Nein, die IPv4-Adressen ändern sich innerhalb eines Netzwerksegments nicht. Änderungen treten auf, wenn Pakete durch Router zwischen verschiedenen Netzwerken weitergeleitet werden, wobei NAT (Network Address Translation) angewendet werden kann.

17. **Wenn Sie der Antwort auf ein Ping folgen, manchmal als Pong bezeichnet, sehen Sie, dass die sendenden und empfangenden IPv4-Adressen wechseln?**  
    A: Ja, beim Ping-Antwortprozess werden die Quell- und Ziel-IPv4-Adressen vertauscht, da die Antwort vom Ziel zurück zum ursprünglichen Sender gesendet wird.

18. **Was ist das Muster der IPv4-Adressierung, die in dieser Simulation verwendet wurde?**  
    A: Die IPv4-Adressierung folgt dem Muster von privaten Netzwerkadressen, die für lokale Netzwerkkommunikation innerhalb eines Unternehmens oder Heimnetzwerks reserviert sind.

19. **Warum müssen verschiedenen IP-Netzwerken unterschiedliche Ports eines Routers zugewiesen werden?**  
    A: Unterschiedliche Ports eines Routers ermöglichen die Segmentierung und Verwaltung des Verkehrs zwischen verschiedenen IP-Netzwerken, wodurch eine effiziente Datenrouting- und Netzwerksicherheitsstrategie ermöglicht wird.

20. **Wenn diese Simulation mit IPv6 anstelle von IPv4 konfiguriert wäre, was wäre anders?**  
    A: Bei Verwendung von IPv6 würden sich die Adressformate ändern, da IPv6 längere Adressen verwendet. IPv6 bietet verbesserte Sicherheitsfunktionen, eine effizientere Routing-Leistung und eliminiert die Notwendigkeit von NAT aufgrund des erheblich größeren Adressraums.