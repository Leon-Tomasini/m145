# Aufageb 5: Packet Tracer - Untersuchung der ARP-Tabelle

## Inhalt
- [Aufageb 5: Packet Tracer - Untersuchung der ARP-Tabelle](#aufageb-5-packet-tracer---untersuchung-der-arp-tabelle)
  - [Inhalt](#inhalt)
  - [Adresstabelle](#adresstabelle)
  - [Ziele](#ziele)
  - [Anweisungen](#anweisungen)
    - [Teil 1: Eine ARP-Anfrage untersuchen](#teil-1-eine-arp-anfrage-untersuchen)
      - [Schritt 1: Generieren Sie ARP-Anfragen](#schritt-1-generieren-sie-arp-anfragen)
      - [Schritt 2: Untersuchen Sie die ARP-Tabelle](#schritt-2-untersuchen-sie-die-arp-tabelle)
    - [Teil 2: Eine Switch MAC-Adresstabelle untersuchen](#teil-2-eine-switch-mac-adresstabelle-untersuchen)
      - [Schritt 1: Generieren Sie zusätzlichen Verkehr](#schritt-1-generieren-sie-zusätzlichen-verkehr)
      - [Schritt 2: Untersuchen Sie die MAC-Adresstabelle auf den Switches](#schritt-2-untersuchen-sie-die-mac-adresstabelle-auf-den-switches)
    - [Teil 3: Den ARP-Prozess bei entfernten Kommunikationen untersuchen](#teil-3-den-arp-prozess-bei-entfernten-kommunikationen-untersuchen)
      - [Schritt 1: Generieren Sie Verkehr für ARP-Traffic](#schritt-1-generieren-sie-verkehr-für-arp-traffic)
      - [Schritt 2: Untersuchen Sie die ARP-Tabelle auf Router1$](#schritt-2-untersuchen-sie-die-arp-tabelle-auf-router1)

## Adresstabelle

| Gerät      | Schnittstelle | MAC-Adresse      | Switch-Interface |
|------------|---------------|------------------|------------------|
| Router0    | Gg0/0         | 0001.6458.2501   | G0/1             |
| Router0    | S0/0/0        | N/A              | N/A              |
| Router1    | G0/0          | 00E0.F7B1.8901   | G0/1             |
| Router1    | S0/0/0        | N/A              | N/A              |
| 10.10.10.2 | Wireless      | 0060.2F84.4AB6   | F0/2             |
| 10.10.10.3 | Wireless      | 0060.4706.572B   | F0/2             |
| 172.16.31.2| F0            | 000C.85CC.1DA7   | F0/1             |
| 172.16.31.3| F0            | 0060.7036.2849   | F0/2             |
| 172.16.31.4| G0            | 0002.1640.8D75   | F0/3             |

## Ziele
- **Teil 1**: Eine ARP-Anfrage untersuchen
- **Teil 2**: Eine Switch MAC-Adresstabelle untersuchen
- **Teil 3**: Den ARP-Prozess bei entfernten Kommunikationen untersuchen

## Anweisungen

### Teil 1: Eine ARP-Anfrage untersuchen

#### Schritt 1: Generieren Sie ARP-Anfragen

- a. Klicken Sie auf 172.16.31.2 und öffnen Sie die Eingabeaufforderung.
  
- b. Geben Sie den Befehl `arp -d` ein, um die ARP-Tabelle zu löschen.
  
- c. Betreten Sie den Simulationsmodus und geben Sie den Befehl `ping 172.16.31.3` ein.

**Q**: Ist diese Adresse in der oben genannten Tabelle aufgelistet?  
**A**: Ja, die MAC-Adresse sollte in der Adresstabelle aufgelistet sein, da sie der Zieladresse des Pings entspricht.

**Q**: Wie viele Kopien der PDU hat Switch1 gemacht?  
**A**: Switch1 macht eine Kopie der ARP-Anfrage für jedes Gerät im lokalen Netzwerk, da es sich um eine Broadcast-Nachricht handelt.

**Q**: Was ist die IP-Adresse des Geräts, das die PDU akzeptiert hat?  
**A**: Die IP-Adresse des akzeptierenden Geräts ist 172.16.31.3, das Ziel der ARP-Anfrage.

**Q**: Was geschah mit den Quell- und Ziel-MAC-Adressen?  
**A**: Die Quell-MAC-Adresse ist die des sendenden Geräts (172.16.31.2), und die Ziel-MAC-Adresse ist die Broadcast-Adresse (ff:ff:ff:ff:ff:ff) für ARP-Anfragen.

**Q**: Wie viele Kopien der PDU hat der Switch während der ARP-Antwort gemacht?  
**A**: Während der ARP-Antwort macht der Switch nur eine Kopie der PDU, die direkt an das anfragende Gerät adressiert ist.

#### Schritt 2: Untersuchen Sie die ARP-Tabelle

- a. Beachten Sie, dass das ICMP-Paket wieder erscheint.
  
- b. Wechseln Sie zurück in den Echtzeitmodus und der Ping wird abgeschlossen.
  
- c. Klicken Sie auf 172.16.31.2 und geben Sie den Befehl `arp -a` ein.

**Q**: Do the MAC addresses of the source and destination align with their IP addresses?  
**A**: Ja, die MAC-Adressen in der ARP-Antwort sollten mit den IP-Adressen übereinstimmen, da die ARP-Antwort die Zuordnung der IP-Adresse zur MAC-Adresse liefert.

**Q**: To what IP address does the MAC address entry correspond?  
**A**: Der MAC-Adresseneintrag entspricht der IP-Adresse des Geräts, von dem die ARP-Antwort gesendet wurde (in diesem Fall 172.16.31.3).

### Teil 2: Eine Switch MAC-Adresstabelle untersuchen

#### Schritt 1: Generieren Sie zusätzlichen Verkehr

- a. Von 172.16.31.2 aus, geben Sie den Befehl `ping 172.16.31.4` ein.
  
- b. Klicken Sie auf 10.10.10.2 und öffnen Sie die Eingabeaufforderung.
  
- c. Geben Sie den Befehl `ping 10.10.10.3` ein.

#### Schritt 2: Untersuchen Sie die MAC-Adresstabelle auf den Switches

- a. Klicken Sie auf Switch1, dann auf den CLI-Reiter. Geben Sie den Befehl `show mac-address-table` ein.
  
- b. Klicken Sie auf Switch0, dann auf den CLI-Reiter. Geben Sie den Befehl `show mac-address-table` ein.

### Teil 3: Den ARP-Prozess bei entfernten Kommunikationen untersuchen

#### Schritt 1: Generieren Sie Verkehr für ARP-Traffic

- a. Klicken Sie auf 172.16.31.2 und öffnen Sie die Eingabeaufforderung.
  
- b. Geben Sie den Befehl `ping 10.10.10.1` ein.
  
- c. Geben Sie `arp -a` ein.

**Q**: Was ist die IP des neuen Eintrags?  
**A**: Die neue ARP-Tabelleneintragung würde die IP-Adresse des Gateways (Router) enthalten, da der Ping eine entfernte Adresse anspricht.
  
- d. Geben Sie `arp -d` ein, um die ARP-Tabelle zu löschen und wechseln Sie in den Simulationsmodus.
  
- e. Wiederholen Sie das Ping zu 10.10.10.1.

**Q**: Wie viele PDUs erscheienn? 
**A**: Zwei PDUs erscheinen in der Regel: eine ARP-Anfrage, um die MAC-Adresse des Gateways zu ermitteln, und das ICMP-Paket für den Ping.

**Q**: Was ist die Ziel-IP?  
**A**: Die Ziel-IP-Adresse der ARP-Anfrage ist die IP-Adresse des Gateways, da es der nächste Hop für die entfernte Kommunikation ist.

**Q**: Wieso ist das Ziel nicht 10.10.10.1?  
**A**: Weil der Ziel-IP-Bereich außerhalb des lokalen Netzwerks liegt, muss das Paket durch das Gateway (Router) zum Zielnetzwerk geleitet werden.

#### Schritt 2: Untersuchen Sie die ARP-Tabelle auf Router1$

- a. Wechseln Sie in den Echtzeitmodus. Klicken Sie auf Router1 und dann auf den CLI-Reiter.

**Q**: Wie viele MAC-Adressen sind in der Tabelle und Wieso?
**A**: Die Anzahl der MAC-Adressen in der Tabelle hängt von der Anzahl der Geräte ab, mit denen der Router kommuniziert hat. Jedes Gerät, das eine ARP-Anfrage stellt oder auf eine solche antwortet, wird eingetragen.

- b. Geben Sie den Befehl `show arp` ein.

**Q**: Gibt es einen Eintrag mit der IP 172.16.31.2?  
**A**: Ja, es sollte einen Eintrag für 172.16.31.2 geben, da von diesem Gerät eine ARP-Anfrage gestellt wurde.

**Q**: Was passiert mit dem ersten ping nach einem ARP?  
**A**: Der erste Ping wird oft "verloren", während das ARP-Verfahren die MAC-Adresse auflöst. Nachfolgende Pings sind erfolgreich, sobald die ARP-Tabelle aktualisiert wurde.