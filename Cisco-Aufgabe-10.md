# Aufagebe 10: Investigate a VLAN Implementation

## Inhalt
- [Aufagebe 10: Investigate a VLAN Implementation](#aufagebe-10-investigate-a-vlan-implementation)
  - [Inhalt](#inhalt)
  - [Adressing Table](#adressing-table)
  - [Objectives](#objectives)
  - [Instructions](#instructions)
    - [Part 1: Observe Broadcast Traffic in a VLAN Implementation](#part-1-observe-broadcast-traffic-in-a-vlan-implementation)
        - [Step 1: Ping from PC1 to PC6](#step-1-ping-from-pc1-to-pc6)
        - [Step 2: Ping from PC1 to PC4](#step-2-ping-from-pc1-to-pc4)
    - [Part 2: Observe Broadcast Traffic without VLANs](#part-2-observe-broadcast-traffic-without-vlans)
        - [Step 1: Clear the configurations on all three switches and delete the VLAN database](#step-1-clear-the-configurations-on-all-three-switches-and-delete-the-vlan-database)
        - [Step 2: Reload the switches](#step-2-reload-the-switches)
        - [Step 3: Click Capture/Forward to send ARP reqeuests and pings](#step-3-click-capture/forward-to-send-arp-requests-and-pings)
    - [Reflection Questions](#reflection-questions)


## Adressing Table

| Device | Interface | IP Adress    | Subnet Mask   | Default Gateway|
|------- |-----------|--------------|---------------|----------------|
| S1     | VLAN 99   | 172.17.99.31 | 255.255.255.0 | N/A            |
| S2     | VLAN 99   | 172.17.99.32 | 255.255.255.0 | N/A            |
| S3     | VLAN 99   | 172.17.99.33 | 255.255.255.0 | N/A            |
| PC1    | NIC       | 172.17.10.21 | 255.255.255.0 | 172.17.10.1    |
| PC2    | NIC       | 172.17.20.22 | 255.255.255.0 | 172.17.20.1    |
| PC3    | NIC       | 172.17.30.23 | 255.255.255.0 | 172.17.30.1    |
| PC4    | NIC       | 172.17.10.24 | 255.255.255.0 | 172.17.10.1    |
| PC5    | NIC       | 172.17.20.25 | 255.255.255.0 | 172.17.20.1    |
| PC6    | NIC       | 172.17.30.26 | 255.255.255.0 | 172.17.30.1    |
| PC7    | NIC       | 172.17.10.27 | 255.255.255.0 | 172.17.10.1    |
| PC8    | NIC       | 172.17.20.28 | 255.255.255.0 | 172.17.20.1    |
| PC9    | NIC       | 172.17.30.29 | 255.255.255.0 | 172.17.30.1    |
## Objectives
- **Part 1**: Observe Broadcast Traffic in a VLAN Implementation
- **Part 2**: Ping from PC1 to PC4

## Instructions

### Part 1: Observe Broadcast Traffic in a VLAN Implementation

#### Step 1: Ping from PC1 to PC6

- a. Wait for all the link lights to turn to green. To accelerate this process, click Fast Forward Time located in the bottom tool bar.

- b. Click the Simulation tab and use the Add Simple PDU tool. Click PC1, and then click PC6.

- c. Click the Capture/Forward button to step through the process. Observe the ARP requests as they traverse the network. When the Buffer Full window appears, click the View Previous Events button.

**Q**: Were the pings successful? Explain.

**A**: Nein, da es in diesem Netzwerk nicht möglich ist, von einem VLAN in ein anderes zu kommunizieren.

**Q**: Look at the Simulation Panel, where did S3 send the packet after receiving it?

**A**: Zum PC5, da dieser ebenfalls im VLAN 10 ist, wie PC1.

#### Step 2: Ping from PC1 to PC4

- a. Click the New button under the Scenario 0 dropdown tab. Now click on the Add Simple PDU icon on the right side of Packet Tracer and ping from PC1 to PC4.

- b. Click the Capture/Forward button to step through the process. Observe the ARP requests as they traverse the network. When the Buffer Full window appears, click the View Previous Events button.

**Q**:Were the pings successful? Explain.

**A**: Nein, da sich PC6 und PC1 in verschiedenen VLANs befinden.

- c. Examine the Simulation Panel.

**Q**: When the packet reached S1, why does it also forward the packet to PC7?

**A**: Das Packet wird an PC7 weitergeleitet, da PC1 und PC7 im selben VLAN (vlan10) sind

### Part 2: Observe Broadcast Traffic without VLANs

#### Step 1: Clear the configurations on all three switches and delete the VLAN database

- a. Return to Realtime mode.

- b. Delete the startup configuration on all 3 switches.

**Q**: What command is used to delete the startup configuration of the switches?

**A**: ´erase startup-configuration´ oder ´write erase´.

**Q**: Where is the VLAN file stored in the switches?

**A**: Die VLAN-Konfigurationen werden in der running-configuration gespeichert, um sie in der startup-configuration zu speichern, muss man den Befehl ´copy running-configuration´ (abgekürzt ´cop r st´) oder ´write memory´ (abgekürzt ´wr´/´wr m´).

- c. Delete the VLAN file on all 3 switches.

**Q**: What command deletes the VLAN file stored in the switches?

**A**: Mit dem Befehl ´erase startup-configuration´ kann man die VLAN-Konfigurationen löschen.

#### Step 2: Reload the switches
- Use the reload command in privileged EXEC mode to reset all the switches. Wait for the entire link to turn green. Toaccelerate this process, click Fast Forward Time located in the bottom yellow tool bar.

#### Step 3: Click Capture/Forward to send ARP requests and pings

- a. After the switches reload and the link lights return to green, the network is ready to forward your ARP and ping traffic.

- b. Select Scenario 0 from the drop-down tab to return to Scenario 0.

- c. From Simulation mode, click the Capture/Forward button to step through the process. Notice that the switches now forward the ARP requests out all ports, except the port on which the ARP request was received. This default action of switches is why VLANs can improve network performance. Broadcast traffic is contained within each VLAN. When the Buffer Full window appears, click the View Previous Events button.

### Reflection Questions

**1**: If a PC in VLAN 10 sends a broadcast message, which devices receive it?

**A**: Wenn PC1 einen Broadcast an 172.17.10.255 macht, erhalten diesen der PC7 und der PC4.

**2**: If a PC in VLAN 20 sends a broadcast message, which devices receive it?

**A**: Gleich wie bei der ersten Frage, alle im selben VLAN, heisst also PC2, PC5 und PC8.

**3**: If a PC in VLAN 30 sends a broadcast message, which devices receive it?

**A**: PC3, PC6 und PC9.

**4**: What happens to a frame sent from a PC in VLAN 10 to a PC in VLAN 30?

**A**: Es wird an alle PCs im VLAN 10 weitergeleitet, kommt jedoch nicht in ein anderes VLAN.